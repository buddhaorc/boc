#include "testApp.h"
#include "ofxIniSettings.h"
#include "Poco/String.h"


//--------------------------------------------------------------
void testApp::setup(){
    cout << "BOC " << __DATE__ << " " << __TIME__ << endl;
    ofSetWindowTitle("Buddha Orchestra Control " __DATE__ " [" + Replicator::getThisIp()+ "]");

    ofDisableArbTex();
    for (int id = 0; id < MAX_MELODIES; ++id)
    {
        melodies[id].m_Id = id;
    }

    ofxIniSettings ini;
    ini.load("bconfig.ini", true);
    gui.init(this, ini);
    gui.showMelodies(false);
    string routeFile = ini.get("routes", string("route.txt"));
    if (!routes.load(routeFile.c_str()))
    {
        cout << "route not loaded: " << routeFile << endl;
    }
	img.loadImage(*routes.getImageName() != 0 ? routes.getImageName() : "image1.jpg");
	ofBackground(0, 0, 0);
	replicator.init(this, ini);
	player.init(this);
	solo = false;
}

//--------------------------------------------------------------
void testApp::update(){
	replicator.invoke();
}

//--------------------------------------------------------------
void testApp::draw() {

    int h = ofGetHeight() - BOTTOM;
    int w = ofGetWidth();
    ofSetHexColor(0xFFFFFF);
    img.draw(0, 0, w, h);
    routes.draw(w, h);
    ofSetHexColor(lastCmd.find("ERROR") == 0 ? 0x770000 : 0x777777);
    ofDrawBitmapString(lastCmd, 10, h + 12);
    ofSetHexColor(0x777777);
    ofDrawBitmapString(playingInfo, w - playingInfo.size() * 8 - 4, h + 12);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

    switch (key)
    {
    case 27: return;
    case 'g':
        gui.toggleVisible();
        break;
    case 'W':
        replicator.getSenders();
        break;
    default:
        lastCmd = replicator.key(key);
    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){
    routes.mouseOver(x, y);
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
    if (gui.isHit(x, y)) return;
    int p = routes.mousePressed(x, y);
    if (p >= 0)
    {
        char buf[100];
        bool pla = melodies[p].getPlayingState() & MPLAY;
        sprintf(buf, "MELODY %d", p);
        gui.setMelodyLabel(buf);
        gui.setMelodyPlaying(pla);
        gui.showMelodies(true);
        lastCmd = replicator.selectMelody(melodyId = p);
    }
    else
    {
        gui.showMelodies(false);
        melodyId = -1;
    }
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

//--------------------------------------------------------------
void testApp::setValue(int key, float v, int me)
{
    if (me < 0) me = melodyId;
    lastCmd = replicator.value(key, v, me);
}

void testApp::setStrValue(int key, string s, int me)
{
    if (me < 0) me = melodyId;
    lastCmd = replicator.svalue(key, s, me);
}

void testApp::selectMelody(int id)
{
    lastCmd = replicator.selectMelody(melodyId = id);
}

void testApp::play()
{
    lastCmd = replicator.play(melodyId);
    melodies[melodyId].start();
}

void testApp::pause()
{
    lastCmd = replicator.pause(melodyId);
    melodies[melodyId].stop();
}

void testApp::exit()
{
    cout << "at exit" << endl;
    if (player.isThreadRunning())
    {
        player.stopThread();
        player.waitForThread(false);
    }
}

void testApp::midiControl(int control, int value)
{

}

void testApp::midiNote(bool on, int ch, int pitch, int velo)
{

}

void testApp::loadImage(string s)
{
    lastCmd = replicator.loadImage(s);
}

void testApp::loadStoredMelodies(string s)
{
    lastCmd = replicator.loadStoredMelodies(s);
}

void testApp::goTo(float x, float y)
{
    lastCmd = replicator.goTo(x, y);
}

void testApp::scale(string scname)
{
    lastCmd = replicator.loadScale(scname);
}

void testApp::error(string e)
{
    lastCmd = "ERROR " + e;
}

void testApp::info(string e)
{
    lastCmd = e;
}

void testApp::playRecord(string recname)
{
    player.addRecord(recname);
    lastCmd = recname;
    if (!player.isThreadRunning())
    {
        player.startThread();
    }
}

void RecordPlayer::threadedFunction()
{
    while (isThreadRunning())
    {
        if (records.size())
        {
            lock();
            string r = records.front();
            records.pop_front();
            unlock();
            cout << "starting " << r << endl;
            playRecord(r);
            cout << "done " << r << endl;
        }
        else
        {
            sleep(1000);
        }
    }
}

void RecordPlayer::scanfile(ifstream &is)
{
    int t;
    string s;
    char c;
    totalLn = 0;
    totalTime = 0;
    while (true)
    {
        if (! ((is) >> t >> c) ) break;
        if (! (getline(is, s)) ) break;
        totalLn++;
        if (totalTime >=0 )
        {
            totalTime = t;
        }
        if (c == 'Z')
        {
            totalTime = -1; // infinite loop
        }
    }
    is.clear();
    is.seekg( 0 );
    currentLn = 0;
}

bool RecordPlayer::readNext(ifstream &is)
{
    int t;
    char c;
    if (! ((is) >> t >> c) ) return false;
    if (! (getline(is, cmd)) ) return false;
    nextAct = c;
    nextActTime = t;
    Poco::trimInPlace(cmd);
    return true;
}

void RecordPlayer::playRecord(string r)
{
    char buf[1000];
    ifstream is(r.c_str());
    if (!is)
    {
        ta->error("file not found " + r);
        return;
    }
    scanfile(is);
    t0 = ofGetElapsedTimeMillis();
    int key, mel, control, value, channel, velocity, pitch;
    float x, y;
    while (readNext(is)) {
        currentLn++;
        while (true)
        {
            long now = ofGetElapsedTimeMillis();
            sprintf(buf, "%s line:%d[%d] %.0fs", r.c_str(), currentLn, totalLn,
                    (totalTime - now + t0) / 1000.0f);
            ta->playingInfo = buf;

            int w = t0 + nextActTime - now;
            if (w > 500)
            {
                sleep(490);
            }
            else
            {
                if (w < 0) break;
            }
            if (!isThreadRunning()) return;
        }
cout << cmd << endl;

        switch (nextAct)
        {
        case 'Z':
            t0 = ofGetElapsedTimeMillis();
            break;
        case '0':
            t0 = ofGetElapsedTimeMillis();
            is.seekg( 0 );
            cout << "seekg " << is.tellg() << endl;
            currentLn = 0;
            break;
        case 'K':
            if (sscanf(cmd.c_str(), "%d", &key))
            {
                ta->keyPressed(key);
//                if (key == 27) exit(0);
            }
            break;
        case 'M':
            if (sscanf(cmd.c_str(), "%d %d", &control, &value))
            {
                ta->midiControl(control, value);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'N':
            if (sscanf(cmd.c_str(), "%d %d %d %d", &channel, &key, &pitch, &velocity) == 4)
            {
                ta->midiNote(key == 0x90, channel, pitch, velocity);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'I':
            ta->loadImage(cmd);
            break;
        case 'V':
            if (sscanf(cmd.c_str(), "%d %d %g", &key, &mel, &x))
            {
                ta->setValue(key, x, mel);
                ta->updateGui(true);
            }
            break;
        case 'A':
            if (sscanf(cmd.c_str(), "%d %d", &key, &mel) == 2)
            {
                size_t sep = cmd.find('=');
                if (sep != string::npos)
                {
                    ta->setStrValue(key, cmd.substr(sep + 1), mel);
                    ta->updateGui(true);
                }
            }
            break;
        case 'S':
            ta->loadStoredMelodies(cmd);
            break;
        case 'G':
            if (sscanf(cmd.c_str(), "%f %f", &x, &y))
            {
                ta->goTo(x, y);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        case 'T':
            if (sscanf(cmd.c_str(), "%d", &key))
            {
                ta->selectMelody(key);
            }
            else
            {
                cout << "ignored " << cmd << endl;
            }
            break;
        }
        nextActTime = 0x7fffffff;
    };
    ta->playingInfo = string("completed ") + r;
}

bool testApp::newOscMessage(ofxOscMessage& m)
{
    int key, mel;
    if(m.getAddress() == "/bo/melody/play"){
        mel = m.getArgAsInt32(0);
        cout << "remote play " << mel << endl;
        melodies[mel].start();
        if (mel == melodyId)
        {
            gui.setMelodyPlaying(true);
        }
    }
    else if(m.getAddress() == "/bo/melody/pause"){
        mel = m.getArgAsInt32(0);
        cout << "remote pause " << mel << endl;
        melodies[mel].stop();
        if (mel == melodyId)
        {
            gui.setMelodyPlaying(false);
        }
    }
    else if(m.getAddress() == "/bo/key"){
        key = m.getArgAsInt32(0);
        if (m.getNumArgs() > 1) mel = m.getArgAsInt32(1);
        cout << "remote key " << key << endl;
        switch (key)
        {
        case 'o':
            solo = !solo;
            break;
        case 'P':
        case 261: // F5
            solo = !solo;
            break;
        default:
            melodies[melodyId].control(key);
        }
    }
    else if(m.getAddress() == "/bo/value"){
        gui.setValue(m.getArgAsInt32(0), m.getArgAsFloat(2), m.getArgAsInt32(1));
        cout << "remote value " << m.getArgAsInt32(0) << endl;
    }
    else if(m.getAddress() == "/bo/svalue"){
        gui.setStrValue(m.getArgAsInt32(0), m.getArgAsString(2), m.getArgAsInt32(1));
        cout << "remote svalue " << m.getArgAsInt32(0) << " '" << m.getArgAsString(2) << "'" << endl;
    }
    else if(m.getAddress() == "/bo/select"){
//        ctl.setCurrentMelody(m.getArgAsInt32(0));
         cout << "remote select " << m.getArgAsInt32(0) << endl;
    }
    else if(m.getAddress() == "/bo/goto"){
        float x = m.getArgAsFloat(0);
        float y = m.getArgAsFloat(1);
//        ctl.getCurrentMelody()->goTo(x, y);
         cout << "remote goto " << x << " " << y << endl;
    }
    else if(m.getAddress() == "/bo/image"){
        string s = m.getArgAsString(0);
//        loadImage(s);
        cout << "remote load image " << s << endl;
    }
    else if(m.getAddress() == "/bo/melodies"){
        cout << "remote load melodies " << m.getArgAsString(0) << endl;
    }
    else if(m.getAddress() == "/bo/midi/control"){
        cout << "remote midi control " << m.getArgAsInt32(0) << " " << m.getArgAsInt32(1) << endl;
    }
    else if(m.getAddress() == "/bo/midi/note"){
         cout << "remote midi note " << m.getArgAsInt32(0) << " " << m.getArgAsInt32(1) << " " << m.getArgAsInt32(2) << endl;
    }
    else if(m.getAddress() == "/bo/route/header"){
         cout << "remote route header " << m.getArgAsInt32(7) << " " << m.getArgAsInt32(1) << " " << m.getArgAsInt32(2) << endl;
         string res = routes.validateHeader(m.getArgAsInt32(1), m.getArgAsInt32(2));
         if (res == "OK")
         {
            res = routes.setRouteHeader(m.getArgAsInt32(3), m.getArgAsInt32(6), ofPoint(m.getArgAsFloat(4), m.getArgAsFloat(5)));
            if (res != "OK")
            {
                error(res);
                cout << res << endl;
            }
         }
         else
         {
             error(res);
             cout << res << endl;
         }
    }
    else if(m.getAddress() == "/bo/route/point")
    {
        string res = routes.addRoutePoint(m.getArgAsInt32(0), m.getArgAsFloat(1), m.getArgAsFloat(2));
        if (res != "OK") error(res);
    }
    else {
        return false;
    }
//    gui.updateGui(true);
    return true;
}
