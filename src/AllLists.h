#ifndef ALLLISTS_H
#define ALLLISTS_H

#define MAX_MIDI 8

#include "ofxIniSettings.h"

class AllLists
{
    public:
        AllLists();
        virtual ~AllLists();
        void init(ofxIniSettings &ini);

        vector<string> &getMidis() { return midis; }
        vector<string> &getDrums() { return drums; }
        vector<string> &getDests() { return dests; }
        vector<string> &getChords() { return chords; }
        vector<string> &getRecords() { return records; }
        vector<string> &getScales() { return scales; }
        vector<string> &getInstruments(int midi) { return instruments[midi]; }

    protected:
    private:
        vector<string> midis;
        vector<string> drums;
        vector<string> dests;
        vector<string> chords;
        vector<string> records;
        vector<string> scales;
        vector<string> instruments[MAX_MIDI];
};

#endif // ALLLISTS_H
