#include "Route.h"
#include "Poco/String.h"

Route::Route()
{
    //ctor
}

Route::Route(int id)
{
    //ctor
    m_Id = id;
}

Route::~Route()
{
    //dtor
}

Routes::Routes()
{
    routes.assign(MAX_ROUTES, (Route*)0);
    m_selected = -1;
    m_mouse = -1;
}

Routes::~Routes()
{
    //dtor
    for (int i=0; i<MAX_ROUTES; ++i)
    {
        if (routes[i]) delete routes[i];
    }
}

bool Route::load(istream &is)
{
    string s;
    if (!getline(is, s) || s.length() == 0) return false;
    vector<string> ss = ofSplitString(s, "|", false, true);
    if (ss.size() < 5)
    {
        cout << "error " << ss.size() << endl;
        for (int i=0; i<ss.size(); ++i)
        {
            cout << ss[i] << endl;
        }
        return false;
    }
    if (!sscanf(ss[0].c_str(), "%d", &m_Id))
    {
        cout << "error " << ss[0] << endl;
        return false;
    }
    if (sscanf(ss[1].c_str(), "%f,%f", &m_center.x, &m_center.y) != 2)
    {
        cout << "error " << ss[1] << endl;
        return false;
    }

    if (!sscanf(ss[2].c_str(), "%d", &m_col))
    {
        cout << "error " << ss[2] << endl;
        return false;
    }

    int num;
    if (!sscanf(ss[3].c_str(), "%d", &num))
    {
        cout << "error " << ss[3] << endl;
        return false;
    }
    Poco::trimInPlace(ss[4]);
    vector<string> pts = ofSplitString(ss[4], ")", true, true);
    if (num != pts.size())
    {
        cout << "error " << num << "!=" << pts.size() << endl;
        return false;
    }

    for (int i=0; i<num; i++)
    {
        int x, y;
        if (sscanf(pts[i].c_str(), "(%d,%d", &x, &y) != 2)
        {
            cout << "error " << pts[i] << endl;
            return false;
        }
        poly.addVertex(x, y);
    }
    return true;
}

bool Route::reset(int id, int col, ofPoint center)
{
    if (id != m_Id)
    {
        cout << id << "!=" << m_Id << endl;
        return false;
    }
    m_center = center;
    m_col = col;
    poly.clear();
}

void Route::addPoint(float x, float y)
{
    poly.addVertex(x, y);
}

void Route::draw(float sx, float sy, bool selected, bool mouse, bool fill)
{
    if (fill)
    {
        ofColor c = ofColor::fromHex(m_col, 127);
        ofSetColor(c);
        ofFill();
    }
    else
    {
        ofColor c = ofColor::fromHex(m_col);
        c.setBrightness(255);
        ofSetColor(c);
        ofNoFill();
        ofSetLineWidth(selected ? 3 : 1);
    }
	ofSetPolyMode(OF_POLY_WINDING_ODD);	// this is the normal mode
	ofBeginShape();
    for (int i = 0; i < poly.size(); ++i)
    {
        ofVertex(poly[i].x * sx, poly[i].y * sy);
    }
	ofEndShape(OF_CLOSE);
	ofSetColor(ofColor::white);
	float cx = m_center.x * sx;
	float cy = m_center.y * sy;
	int A = selected ? 6 : 2;
	ofLine(cx - A, cy, cx + A, cy);
	ofLine(cx, cy - A, cx, cy + A);
	char buf[10];
	sprintf(buf,"%d", m_Id);
	if (mouse)
    {
        ofDrawBitmapStringHighlight(buf, ofPoint(cx, cy + 16));
    }
    else
    {
        ofDrawBitmapString(buf, ofPoint(cx, cy + 16));
    }
}

bool Routes::load(const char* fn)
{
    ifstream is(fn);
    char c;
    if (!is)
    {
        return false;
    }
    if (!(is >> imgW)) return false;
    is >> c; // , separator
    if (!(is >> imgH)) return false;

    is >> c;
    getline(is, imageFileName);
    Poco::trimInPlace(imageFileName);
    cout << imgW << " x " << imgH << " '" << imageFileName << "'" << endl;
    int cnt = 0;

    while (true)
    {
        Route *r = new Route();
        if (!r->load(is))
        {
            delete r;
            break;
        }
        if (r->m_Id >= MAX_ROUTES)
        {
            cout << "Route ID out of range, ignored " << r->m_Id << endl;
            delete r;
            continue;
        }
        routes[r->m_Id] = r;
        ++cnt;
    }
    return cnt != 0;
}

string Routes::validateHeader(int wd, int ht)
{
    if (!imgW && !imgH) // this is the first route
    {
        imgW = wd;
        imgH = ht;
        return "OK";
    }
    if (wd == imgW && ht == imgH) return "OK";
    return "Incorrect image size " + ofToString(wd) + "x" + ofToString(ht) +
        " expected " + ofToString(imgW) + "x" + ofToString(imgH);
}


void Routes::draw(float w, float h)
{
    ofPushStyle();
    float sx = w / imgW;
    float sy = h / imgH;
    ofEnableAlphaBlending();
    for (int i=0; i<MAX_ROUTES; ++i)
    {
        if (routes[i])
        {
            routes[i]->draw(sx, sy, m_selected == i, m_mouse == i, true);
        }
    }
    for (int i=0; i<MAX_ROUTES; ++i)
    {
        if (routes[i])
        {
            routes[i]->draw(sx, sy, m_selected == i, m_mouse == i, false);
        }
    }
    ofPopStyle();
}

void Routes::mouseOver(int x, int y)
{
    x *= ((float)imgW / ofGetWidth());
    y *= ((float)imgH / (ofGetHeight() - BOTTOM));
    m_mouse = closest(x, y);
}

int Routes::mousePressed(int x, int y)
{
    x *= ((float)imgW / ofGetWidth());
    y *= ((float)imgH / (ofGetHeight() - BOTTOM));
    if ((m_selected = closest(x, y)) < 0) return -1;
    return routes[m_selected]->m_Id;
}

int Routes::closest(int x, int y)
{
    float d = 1e30;
    int idx = -1;
    for (int i=0; i<MAX_ROUTES; ++i)
    {
        if (!routes[i]) continue;
        ofPoint c = routes[i]->m_center;
        float dist = c.distanceSquared(ofPoint(x, y));
//        pow(c.x - x, 2) + pow(c.y - y, 2);
        if (dist < d)
        {
            d = dist;
            idx = i;
        }
    }
    return d < 100 ? idx : -1;
}

string Routes::setRouteHeader(int id, int col, ofPoint center)
{
    if (id >= MAX_ROUTES)
    {
        return "ERROR: Route ID out of range, ignored " + ofToString(id);
    }
    Route *r = routes[id];
    if (!r)
    {
        r = new Route(id);
        routes[id] = r;
        cout << "added " << id << endl;
    }
    r->reset(id, col, center);
    return "OK";
}

string Routes::addRoutePoint(int id, float x, float y)
{
    if (id >= MAX_ROUTES || !routes[id])
    {
        return "ERROR: Route ID out of range, or route not found " + ofToString(id);
    }
    routes[id]->addPoint(x, y);
    return "OK";
}
