#include "Melody.h"
#include "testApp.h"

using namespace std;

Melody::GenType Melody::gtype = Melody::VERT_LINES;
int Melody::echoDelay = 2000;
int Melody::echoVelocity = -6;
int Melody::echoPitch = 0;

string Melody::storedKeys = "ruiasjzvmU0123456789ABCDEFGZLVN";

//int MidiOut::selected = 0;
//int MidiOut::num = 0;

static char buf[MAXTOKEN];

Melody * Melody::copy(int id)
{
    Melody *m = new Melody();

    list<CPoint*>::iterator pit = points.begin();
    for ( ; pit != points.end(); pit++)
    {
        CPoint *p = *pit;
        m->addPoint(new CPoint(p->x, p->y));
    }

    m->m_Id = id;
    m->boundingRect = boundingRect;
    m->setCenter(centroid);
    if (m->maxDist)
    {
        m_HasChild = true;
        m->m_ParentId = m_ParentId < 0 ? m_Id : m_ParentId;
        m->accord = accord;
        m->ccFlags = ccFlags;
        m->m_Bank = m_Bank;
        m->m_ImgHeight = m_ImgHeight;
        m->m_ImgWidth = m_ImgWidth;
        m->quality = quality;
        m->m_Ch = m_Ch;
        m->m_Ch0 = m_Ch0;
        m->drums = drums;
        m->effect = effect;
        m->inserted = inserted; // in the short list
        m->duration = duration; // total duration of all notes
        m->paused = paused;
        m->vibrato = vibrato;
        m->portamento = portamento;
        m->reverse = reverse;
        m->volume = volume; // adjustment -127..+127

        m->m_Offset = m_Offset; // notes up-down - adjusted with i,j

        m->m_legato = m_legato;
        m->keystory = keystory;
        m->ptrnIdx = ptrnIdx;
        m->m_Ptrn = m_Ptrn;

        return m;
    }
    else
    {
        delete m;
        return 0;
    }

}

Melody::~Melody()
{
    //dtor
    list<CPoint*>::iterator pit = points.begin();
    for ( ; pit != points.end(); pit++)
    {
        CPoint *p = *pit;
        delete p;
    }
}

int chordToArray(list<int> &chord, int *dest, int n0, int cType)
{
    int nn = 0;
    switch (cType)
    {
    case 1: // UP
        for (list<int>::iterator ci = chord.begin(); ci != chord.end(); ci++)
        {
            int no = *ci;
            if (no < n0) no += 12;
            dest[nn++] = no;
        }
        break;
    case 2: // DOWN
        for (list<int>::iterator ci = chord.end(); ci != chord.begin(); )
        {
            --ci;
            int no = *ci;
            if (no < n0) no += 12;
            dest[nn++] = no;
        }
        break;
    case 3: // RND
        nn = chordToArray(chord, dest, n0, 1);
        break;
    }

    return nn;
}

string Melody::getDestName(int i)
{
    switch (i)
    {
        case MIDI: return "MIDI";
        case OSC: return "OSC";
        case BOTH: return "MIDI+OSC";
        case SERIAL: return "SERIAL";
    }
    return "";
}

string Melody::getDestName()
{
    return getDestName(m_Dest);
}

void Melody::setDest(string nm)
{
    if (nm == "MIDI") m_Dest = MIDI;
    else if (nm == "OSC") m_Dest = OSC;
    else if (nm == "MIDI+OSC") m_Dest = BOTH;
    else if (nm == "SERIAL") m_Dest = SERIAL;
}

string Melody::getChordName(int i)
{
    switch (i)
    {
        case 0: return "None";
        case 1: return "Up";
        case 2: return "Down";
        case 3: return "Random";
    }
    return "";
}

string Melody::getChordName()
{
    return getChordName(accord);
}

void Melody::setChord(string nm)
{
    if (nm == "None") accord = 0;
    else if (nm == "Up") accord = 1;
    else if (nm == "Down") accord = 2;
    else if (nm == "Random") accord = 3;
}

void Melody::ccFlagsInfo(char *buf)
{
    char *p = buf;
    if (ccFlags & CC_ENVE) *p++ = 'L';
    if (ccFlags & CC_NOTE) *p++ = 'N';
    if (ccFlags & CC_TRCK) *p++ = 'K';
    if (ccFlags & CC_VELO) *p++ = 'V';
    if (ccFlags & CC_MUTE) *p++ = 'U';
    *p = 0;
}

bool Melody::control(int key)
{
    int val;
    storeKey(key);
    // cout << "mel " << m_Id << " key " << key << endl;
    switch(key)
    {
        case 'p': // play or pause
            if ((paused = !paused))
            {
                stop();
            }
            else
            {
                start();
            }
            break;
        case 'f':
            full = !full;
            break;

        case 'V':
            toggleCCflag(CC_VELO);
            break;
        case 'K':
            toggleCCflag(CC_TRCK);
            break;
        case 'N':
            toggleCCflag(CC_NOTE);
            break;
        case 'L':
            toggleCCflag(CC_ENVE);
            break;
        case 'u':
            toggleCCflag(CC_MUTE);
            break;
        case 'a':
            if (++accord == 4) accord = 0;
            break;
        case 'b':
            reverse = !reverse;
            break;
        // fixed channel assignment
        case '0': case '1': case '2': case '3':
        case '4': case '5': case '6': case '7':
        case '8': case '9':
            drums = false;
            m_Ch0 = key - '0';
            break;
        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F': case 'G':
            drums = key == 'A';
            m_Ch0 = key - 'A' + 10;
            break;
        case '(':
            if (m_legato > 0.1) {
               m_legato -= 0.1;
            }
//            if (m_Bank) {
//               m_Midi->sendBankSelect(m_Ch0, --m_Bank);
//            }
            break;
        case ')':
            if (m_legato < 1.5) {
               m_legato += 0.1;
            }
//            m_Midi->sendBankSelect(m_Ch0, ++m_Bank);
            break;
        case 'v':
            effect = !effect;
            val = effect ? 127 : 0;
//            m_Midi->sendControlChange(m_Ch, MidiInstruments::m_Effect, val);
//            cout << "ch=" << m_Ch << " effect " << MidiInstruments::m_Effect << " val=" << val << endl;
            break;
        case 'i':
            ++m_Offset;
            break;
        case 'j':
            --m_Offset;
            break;
        case 267: // F11
            if (m_dynamic >= 0.05) m_dynamic -= 0.05; else m_dynamic = 0;
            break;
        case 268: // F12
            if (m_dynamic <= 0.95) m_dynamic += 0.05; else m_dynamic = 1;
            break;
        case 'H':
            if (m_echo > 0) --m_echo;
            break;
        case 'J':
            ++m_echo;
            break;
        case 'T':
            stop();
            switch (m_Dest)
            {
                case MIDI: m_Dest = OSC; break;
                case OSC: m_Dest = BOTH; break;
                case BOTH: m_Dest = SERIAL; break;
                case SERIAL: m_Dest = MIDI; break;
            }
            break;
        case ':':
            vibrato = !vibrato;
            val = vibrato ? 127 : 0;
  //          m_Midi->sendControlChange(m_Ch, 1, val);
            cout << "Mod wheel " << val << endl;
            break;
        case ';':
            portamento = !portamento;
            val = portamento ? 127 : 0;
//            m_Midi->sendControlChange(m_Ch, 5, val); // time coarse
//            m_Midi->sendControlChange(m_Ch, 65, val); // on/off
            cout << "Portamento " << val << endl;
            break;
/*
1 Modulation wheel
7 Volume
10 Pan
11 Expression
64 Sustain pedal
91 Reverb level
92 Tremolo level
93 Chorus level
94 Celeste level
95 Phaser level
Filter Resonance (Timbre/Harmonic Intensity) (cc#71)
Release Time (cc#72)
Attack time (cc#73)
Brightness/Cutoff Frequency (cc#74)
Decay Time (cc#75)
Vibrato Rate (cc#76)
Vibrato Depth (cc#77)
Vibrato Delay (cc#78)
*/
    default:
        cout << (char)key << endl;
        return false;
    }

    return true;
}

void Melody::toggleCCflag(int fl) {
    if (ccFlags & fl) {
        ccFlags &= (~fl);
    } else {
        ccFlags |= fl;
    }
}

Ptrn::Ptrn(string p)
{
    char *t = new char[p.length() + 1];
    strcpy(t, p.c_str());
    char *tok = strtok(t, ":");
    strncpy(title, tok, sizeof(title) - 1);
    cout << title << ":";
    int a,b;
    int cnt = 0;
    while ((tok = strtok(0, ";")) != 0 && sscanf(tok, "%i,%i", &a, &b) == 2)
    {
        PtrnItem * pt = new PtrnItem();
        pt->dur = a;
        pt->vol = b;
        pt->cnt = cnt++;
        ptrn.push_back(pt);
        cout << a << "," << b << ";";
    }
    cout << endl;
    delete [] t;
    now = ptrn.begin();
}

Ptrn::~Ptrn()
{
    for (now = ptrn.begin(); now != ptrn.end(); now++)
    {
        PtrnItem *pi = (*now);
        delete pi;
    }
}

PtrnItem *Ptrn::getNext()
{
    PtrnItem *pi = *now;
    if (++now == ptrn.end()) now = ptrn.begin();
    return pi;
}

PatternSet::PatternSet()
{
    m_Cnt = 0;
    memset(ptrns, 0, sizeof(ptrns));
}

void PatternSet::init(ofxIniSettings &ini)
{
    // No assignment to ptrs[0] - melody is generated patternless
    while (m_Cnt++ < MAXPTRN)
    {
        char buf[10];
        sprintf(buf, "r%i", m_Cnt);
        string p = ini.get(buf, string("NONE"));
        if (p == "NONE") break;
        Ptrn *ptr = new Ptrn(p);
        ptrns[m_Cnt] = ptr;
    }
}

PatternSet::~PatternSet()
{
    for(int i=0; i<MAXPTRN; ++i) delete ptrns[i];
}

void Melody::setCenter(ofPoint &c)
{
    centroid = c;
    float md = 0;
    list<CPoint*>::iterator nit = points.begin();
    for ( ; nit != points.end(); nit++)
    {
        CPoint *p = *nit;
        ofPoint pt(p->x, p->y);
        float d = pt.distance(centroid);
        if (d > md) md = d;
    }
    maxDist = md;
}

bool Melody::isDrumming()
{
    return m_DrumCount != 0;
}

//--------------------------------------------------------------
int Melody::getPlayingState()
{
    int st = 0;
    if (!paused) st |= MPLAY;
    if (isDrumming()) st |= MDRUM;
    if (inserted) st |= MINST;
    return st;
}

//--------------------------------------------------------------
void Melody::storeKey(int k)
{
    char c = (char)k;
    if (storedKeys.find(c) != string::npos)
    {
        keystory += c;
    }
}

//--------------------------------------------------------------
bool askContinue() {
    cout << "Continue? (y/n) ";
    char answer;
    cin >> answer;
    return answer != 'n' && answer != 'N';
}
