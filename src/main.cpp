#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "glut.h"

//========================================================================
int main( ){

    ofAppGlutWindow window;
	ofSetupOpenGL(&window, 640,480, OF_WINDOW);			// <-------- setup the GL context
	glutSetCursor(GLUT_CURSOR_CROSSHAIR);

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new testApp());

}
