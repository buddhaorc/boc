#include "AllLists.h"

const char *names[] = {
    // 1..20
    "Acoustic Grand Piano", "Bright Acoustic Piano", "Electric Grand Piano", "Honky-tonk Piano", "Electric Piano 1",
    "Electric Piano 2", "Harpsichord", "Clavi", "Celesta", "Glockenspiel",
    "Music Box", "Vibraphone", "Marimba", "Xylophone", "Tubular Bells",
    "Dulcimer", "Drawbar Organ", "Percussive Organ", "Rock Organ", "Church Organ",
    // 21..40
    "Reed Organ", "Accordion", "Harmonica", "Tango Accordion","Acoustic Guitar (nylon)",
    "Acoustic Guitar (steel)", "Electric Guitar (jazz)", "Electric Guitar (clean)", "Electric Guitar (muted)", "Overdriven Guitar",
    "Distortion Guitar", "Guitar harmonics", "Acoustic Bass", "Electric Bass (finger)", "Electric Bass (pick)",
    "Fretless Bass", "Slap Bass 1", "Slap Bass 2", "Synth Bass 1", "Synth Bass 2",
    // 41..60
    "Violin", "Viola", "Cello", "Contrabass", "Tremolo Strings",
    "Pizzicato Strings", "Orchestral Harp", "Timpani", "String Ensemble 1", "String Ensemble 2",
    "SynthStrings 1", "SynthStrings 2", "Choir Aahs", "Voice Oohs", "Synth Voice",
    "Orchestra Hit", "Trumpet", "Trombone", "Tuba", "Muted Trumpet",
    // 61..80
    "French Horn", "Brass Section", "SynthBrass 1", "SynthBrass 2", "Soprano Sax",
    "Alto Sax", "Tenor Sax", "Baritone Sax", "Oboe", "English Horn",
    "Bassoon", "Clarinet", "Piccolo", "Flute", "Recorder",
    "Pan Flute", "Blown Bottle", "Shakuhachi", "Whistle", "Ocarina",
    // 81..100
    "Lead 1 (square)", "Lead 2 (sawtooth)", "Lead 3 (calliope)", "Lead 4 (chiff)", "Lead 5 (charang)",
    "Lead 6 (voice)", "Lead 7 (fifths)", "Lead 8 (bass + lead)", "Pad 1 (new age)", "Pad 2 (warm)",
    "Pad 3 (polysynth)", "Pad 4 (choir)", "Pad 5 (bowed)", "Pad 6 (metallic)", "Pad 7 (halo)",
    "Pad 8 (sweep)", "FX 1 (rain)", "FX 2 (soundtrack)", "FX 3 (crystal)", "FX 4 (atmosphere)",
    // 101..120
    "FX 5 (brightness)", "FX 6 (goblins)", "FX 7 (echoes)", "FX 8 (sci-fi)", "Sitar",
    "Banjo", "Shamisen", "Koto", "Kalimba", "Bag pipe",
    "Fiddle", "Shanai", "Tinkle Bell", "Agogo", "Steel Drums",
    "Woodblock", "Taiko Drum", "Melodic Tom", "Synth Drum", "Reverse Cymbal",
    // 121..128
    "Guitar Fret Noise", "Breath Noise", "Seashore", "Bird Tweet", "Telephone Ring",
    "Helicopter", "Applause", "Gunshot"
};

// 35..81
const char *drumNames[] = {
    "Acoustic Bass Drum", "Bass Drum 1", "Side Stick", "Acoustic Snare", "Hand Clap",
    // 41..60
    "Electric Snare", "Low Floor Tom", "Closed Hi Hat", "High Floor Tom", "Pedal Hi-Hat",
    "Low Tom", "Open Hi-Hat", "Low-Mid Tom", "Hi-Mid Tom", "Crash Cymbal 1",
    "High Tom", "Ride Cymbal 1", "Chinese Cymbal", "Ride Bell", "Tambourine",
    "Splash Cymbal", "Cowbell", "Crash Cymbal 2", "Vibraslap",
    // 61..80
    "Ride Cymbal 2", "Hi Bongo", "Low Bongo", "Mute Hi Conga", "Open Hi Conga",
    "Low Conga", "High Timbale", "Low Timbale", "High Agogo", "Low Agogo",
    "Cabasa", "Maracas", "Short Whistle", "Long Whistle", "Short Guiro",
    "Long Guiro", "Claves", "Hi Wood Block", "Low Wood Block", "Mute Cuica",

    "Open Cuica", "Mute Triangle", "Open Triangle"
};

AllLists::AllLists()
{
    //ctor
}

AllLists::~AllLists()
{
    //dtor
}

void AllLists::init(ofxIniSettings &ini)
{
    char buff[256];
    records = ofSplitString(ini.get("records", string("")), ",", true, true);
    scales = ofSplitString(ini.get("scales", string("")), ",", true, true);
    chords = ofSplitString(ini.get("chords", string("None,Up,Down,Random")), ",", true, true);
    dests = ofSplitString(ini.get("dests", string("MIDI,OSC,MIDI+OSC,SERIAL")), ",", true, true);
    drums = ofSplitString(ini.get("drumkit", string("D1,D2,DR3")), ",", true, true);
    for (int i=0; i<drums.size(); ++i)
    {
#ifdef DRUM_NAMES
        sprintf(buff, "%i %s", i+1, drums[i].c_str());
#else
        int n;
        if (sscanf(drums[i].c_str(), "%d", &n) && (n >= 35 && n <= 81))
        {
            sprintf(buff, "%i %s", i+1, drumNames[n - 35]);
        }
#endif
        drums[i] = buff;
    }

    midis = ofSplitString(ini.get("midis", string("m1,m2")), ",", true, true);
    for (int i = 0; i < MAX_MIDI; i++)
    {
        string key = "instruments";
        if (i)
        {
            sprintf(buff, "%d", i);
            key += buff;
        }

        string val = ini.get(key, string(""));
        if (!val.length()) break;

        instruments[i].push_back("0 OSC");

        vector<string> ns = ofSplitString(val, ",", true, true);
        for (int j=0; j<ns.size(); ++j)
        {
            int n;
            if (j == 9)
            {
                instruments[i].push_back("{DRUMS}");
            }
            else if (sscanf(ns[j].c_str(), "%d", &n) && (n > 0) && (n <= sizeof(names) / sizeof(names[0])))
            {
                instruments[i].push_back(names[n - 1]);
            }
        }
    }
}
