#include "Gui.h"
#include "testApp.h"

#define CANVAS_HEIGHT 30

Gui::Gui()
{
    //ctor
    wd = 200 - 2 * OFX_UI_GLOBAL_WIDGET_SPACING;
    show = true;
}

Gui::~Gui()
{
    //dtor
}

void Gui::init(testApp *a, ofxIniSettings &ini)
{
    al.init(ini);

    ta = a;
    guiR = guiP = guiI = 0;    // to be filled in update()

    guiG = new ofxUISuperCanvas("GLOBAL", 0, 0, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
    guiG->addSpacer();
    pause = guiG->addToggle("PAUSE", true);
//    tickSlider = guiG->addSlider("TICK", 20, 250, 100);
    tickSlider = new ofxUIMinimalSlider("TICK", 20, 250, 100, 150, 0, OFX_UI_FONT_MEDIUM);
    guiG->addWidgetDown(tickSlider);
    tickSlider->setLabelPrecision(0);

//    bpmSlider  = guiG->addSlider("BPM", 20, 200, 60);
    bpmSlider = new ofxUIMinimalSlider("BPM", 20, 200, 60, 150, 0, OFX_UI_FONT_MEDIUM);
    guiG->addWidgetDown(bpmSlider);
    bpmSlider->setLabelPrecision(0);

    vector<string> devices = al.getMidis();
    if (devices.size() > 1)
    {
        guiG->addSpacer();
        guiG->addLabel("DEVICE");
        midiList = guiG->addRadio("DEVICE", devices);
        midiList->activateToggle(devices[0]);
    }
    else
    {
        midiList = 0;
    }

    guiM = new ofxUISuperCanvas("MELODY", 0, CANVAS_HEIGHT, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
    guiM->addSpacer();
    play = guiM->addLabelToggle("PLAY", true, true);
    play->setColorFill(ofColor(127,255,255,150));
    play->setColorBack(ofColor(255,127,255,200));

    reduceBtn = guiM->addLabelButton("REDUCE (r)", false, true);
    mergeBtn = guiM->addLabelButton("MERGE (m)", false, true);
    splitBtn = guiM->addLabelButton("SPLIT (s)", false, true);
    pauseBtn = guiM->addLabelButton("ADD PAUSES (z)", false, true);
    rmPauseBtn = guiM->addLabelButton("CLEAR PAUSES (Z)", false, true);
    restoreBtn = guiM->addLabelButton("RESTORE (Y)", false, true);

    guiM->addSpacer();

    soloToggle = guiM->addLabelToggle("SOLO (o)", true, true);
    muteToggle = guiM->addLabelToggle("MUTE (u)", true, true);
    reverseToggle = guiM->addLabelToggle("REVERSE (b)", true, true);

    guiM->addSpacer();
    volumeSlider = new ofxUIMinimalSlider("VOLUME", -127, 127, 0.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(volumeSlider);
    volumeSlider->setLabelPrecision(0);
    volumeSlider->setIncrement(1);

    dynamSlider = new ofxUIMinimalSlider("DYNAMIC", 0, 1, 0.5, 150, 0, OFX_UI_FONT_LARGE);
    guiM->addWidgetDown(dynamSlider);
    dynamSlider->setLabelPrecision(2);
    dynamSlider->setIncrement(0.05);

    echoSlider = new ofxUIMinimalSlider("ECHO", 0, 5, 0.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(echoSlider);
    echoSlider->setLabelPrecision(0);
    echoSlider->setIncrement(1);

    offsetSlider = new ofxUIMinimalSlider("TRANSPOSE", -36, 36, 0.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(offsetSlider);
    offsetSlider->setLabelPrecision(0);
    offsetSlider->setIncrement(1);

    qualitySlider = new ofxUIMinimalSlider("QUALITY", 0, 100, 50.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(qualitySlider);
    qualitySlider->setLabelPrecision(0);
    qualitySlider->setIncrement(1);

    stacattoSlider = new ofxUIMinimalSlider("DURATION", 0, 2.0, 1.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(stacattoSlider);
    stacattoSlider->setLabelPrecision(1);
    stacattoSlider->setIncrement(0.1);

    drumSlider = new ofxUIMinimalSlider("DRUM", 0, 5, 0.0, 150, 0, OFX_UI_FONT_MEDIUM);
    guiM->addWidgetDown(drumSlider);
    drumSlider->setLabelPrecision(0);
    drumSlider->setIncrement(1);

    guiS = new ofxUISuperCanvas("SETTINGS", 0, 2 * CANVAS_HEIGHT, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
    guiS->addSpacer();

    guiS->addLabel("MIDI CC");
    envelToggle = guiM->addLabelToggle("ENVELOPE (L)", true, true);
    veloToggle = guiM->addLabelToggle("VELOCITY (V)", true, true);
    noteToggle = guiM->addLabelToggle("NOTE (N)", true, true);
    trackToggle = guiM->addLabelToggle("TRACK (K)", true, true);

    guiS->addSpacer();
    guiS->addLabel("CHORD");
    chordList = guiS->addRadio("CHORD", al.getChords());
    chordList->activateToggle(al.getChords()[0]);

    guiS->addSpacer();
    guiS->addLabel("DESTINATION");
    vector<string> &dd = al.getDests();
    destList = guiS->addRadio("DESTINATION", dd);
    destList->activateToggle(al.getDests()[0]);

    guiD = new ofxUISuperCanvas("DRUMS", 0, 3 * CANVAS_HEIGHT, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
    guiD->addSpacer();
    vector<string> &drums = al.getDrums();
    drumList = guiD->addRadio("DRUMS", drums);

    ofxUISuperCanvas *gg[] = {guiG, guiM, guiS, guiI, guiD, guiP};
    for (int i=0; i<sizeof(gg)/sizeof(gg[0]); i++)
    {
        if (gg[i])
        {
            initCanvas(gg[i]);
        }
    }

    if (al.getRecords().size() || al.getScales().size())
    {
        guiR = new ofxUISuperCanvas("RECORDS", 430, 0, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
    }

    if (al.getRecords().size())
    {
        guiR->addSpacer();
        recordsList = guiR->addRadio("RECORDS", al.getRecords());
    }

    if (al.getRecords().size())
    {
        guiR->addSpacer();
        scalesList = guiR->addRadio("SCALES", al.getScales());
    }

    if (guiR)
    {
        guiR->autoSizeToFitWidgets();
        guiR->setDeltaTime(0.5);
        ofAddListener(guiR->newGUIEvent,this,&Gui::guiEvent);
    }

    if (devices.size())
    {
        updateInstrumentList(devices[ta->midiNo = 0]);
    }

}

void Gui::initCanvas(ofxUISuperCanvas *gg)
{
    gg->setVisible(true);
    gg->setMinified(true);
    gg->autoSizeToFitWidgets();
    gg->setDeltaTime(0.5);
    ofAddListener(gg->newGUIEvent,this,&Gui::guiEvent);
    accordion.addCanvas(gg);
}

//--------------------------------------------------------------
void Gui::updatePlaying()
{
    if (guiP)
    {
        ofRemoveListener(guiP->newGUIEvent,this,&Gui::guiEvent);
        delete guiP;
        guiP = 0;
    }

    vector<string> playing;
    vector<string> drumming;

    char buf[256];
    string activeP, activeD;
    for (int i=0; i<MAX_MELODIES;++i)
    {
        Melody *m = ta->getMelody(i);

        if (m->getPlayingState() & (MPLAY | MINST))
        {
            sprintf(buf, "%2i %s", m->m_Id, al.getInstruments(m->getCh() >> 8)[m->getCh() & 0xFF].c_str());
            playing.push_back(buf);
            if (i == ta->melodyId)
            {
                activeP = buf;
            }
        }
        if (m->getPlayingState() & MDRUM)
        {
            sprintf(buf, "%2i %s", m->m_Id, al.getDrums()[m->m_DrumType].c_str());
            drumming.push_back(buf);
            if (i == ta->melodyId)
            {
                activeD = buf;
            }
        }
    }

    if (drumming.size() || playing.size())
    {
//        guiP = new ofxUISuperCanvas(guiG->getRect()->getMinX(), guiG->getRect()->getMaxY(), 200, ofGetHeight());
        guiP = new ofxUISuperCanvas("PLAYING", guiG->getRect()->getMinX(), guiG->getRect()->getMaxY(), OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
        guiP->addSpacer(wd, 10);
        if (playing.size())
        {
            guiP->addLabel("PLAYING");
            playingList = guiP->addRadio("PLAYING", playing);
            playingList->activateToggle(activeP);
            guiP->addSpacer(wd, 2);
        }
        else
        {
            playingList = 0;
        }

        if (drumming.size())
        {
            guiP->addLabel("DRUMMING");
            drummingList = guiP->addRadio("DRUMMING", drumming);
            if (activeD.length())
            {
                drummingList->activateToggle(activeD);
            }
            guiP->addSpacer(wd, 2);
        }
        else
        {
            drummingList = 0;
        }

        guiP->autoSizeToFitWidgets();
        guiP->setVisible(guiM->isVisible());
        ofAddListener(guiP->newGUIEvent,this,&Gui::guiEvent);
    }
    else
    {
        playingList = 0;
        drummingList = 0;
    }
}

//--------------------------------------------------------------
void Gui::updateGui(bool pla)
{
    Melody *me = ta->getMelody(ta->melodyId);
    if (!me)
    {
        showMelodies(false);
        return;
    }
    bool pl = me->getPlayingState() & MPLAY;
    play->setValue(pl ? 1 : 0);
    play->getLabelWidget()->setLabel(pl ? "PAUSE" : "PLAY");

    bool paused = ta->paused;
    pause->setValue(paused ? 1 : 0);
    pause->getLabelWidget()->setLabel(paused ? "CONTINUE" : "PAUSE");

    envelToggle->setValue(me->getCCflag(CC_ENVE));
    veloToggle->setValue(me->getCCflag(CC_VELO));
    noteToggle->setValue(me->getCCflag(CC_NOTE));
    trackToggle->setValue(me->getCCflag(CC_TRCK));
    muteToggle->setValue(me->getCCflag(CC_MUTE));
    soloToggle->setValue(ta->solo);
    reverseToggle->setValue(me->getReverse());
    reverseToggle->getLabelWidget()->setLabel(me->getReverse() ? "FORTH" : "BACK");
    volumeSlider->setValue(me->getVolume());
    dynamSlider->setValue(me->getDynamic());
    echoSlider->setValue(me->getEcho());
    offsetSlider->setValue(me->getOffset());
    stacattoSlider->setValue(me->getLegato());
    qualitySlider->setValue(me->quality);
    drumSlider->setValue(me->m_DrumCount);

    int t = me->m_DrumType;
    ofxUIToggle *tt = drumList->getToggles()[t];
    if (tt)
    {
        drumList->activateToggle(tt->getName());
//        drumList->setLabelText(tt->getName());
    }

    if (pla)
    {
        // currently playing and drumming
        updatePlaying();
    }

    selectInstrument(me->getCh());

    char buf[100];
    sprintf(buf, "MELODY %d", me->m_Id);
    setMelodyLabel(buf);
    destList->activateToggle(me->getDestName());
    chordList->activateToggle(me->getChordName());
    echoSlider->setVisible(me->getChordName() == "None");
}

void Gui::selectInstrument(int ch0) {
    if (ch0 >= 0 && (ta->midiNo == (ch0 >> 8)))
    {
        int ch = (ch0 & 0xff) ? ((ch0 - 1) & 0xf) + 1 : 0;
        ofxUIToggle *a = instrList->getToggles()[ch];
        if (a)
        {
            instrList->activateToggle(a->getName());
        }
    }
    else
    {
        ofxUIToggle *a = instrList->getActive();
        if (a) a->setValue(false);
    }
}

bool Gui::isHit(float x, float y) {
    return guiM->isHit(x, y) ||
    (guiI && guiI->isHit(x, y)) ||
    (guiP && guiP->isHit(x, y)) ||
    guiS->isHit(x, y) ||
    guiD->isHit(x, y) ||
    guiG->isHit(x, y);
}

void Gui::toggleVisible() {
    guiM->toggleVisible();
    if (guiI) guiI->toggleVisible();
    if (guiP) guiP->toggleVisible();
    guiS->toggleVisible();
    guiD->toggleVisible();
    guiG->toggleVisible();
}

void Gui::showMelodies(bool show) {
    guiM->setVisible(show);
    if (guiI) guiI->setVisible(show);
    if (guiP) guiP->setVisible(show);
    guiS->setVisible(show);
    guiD->setVisible(show);
}

void Gui::updateInstrumentList(string dev)
{
    if (midi != dev)
    {
        cout << dev << endl;
        midi = dev;
        if (guiI)
        {
            ofRemoveListener(guiI->newGUIEvent,this,&Gui::guiEvent);
            delete guiI;
        }

        guiI = new ofxUISuperCanvas("INSTRUMENTS", 0, 4 * CANVAS_HEIGHT, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_GLOBAL_CANVAS_WIDTH, OFX_UI_FONT_MEDIUM);
        guiI->addSpacer();
        int midiNo = ofFind(al.getMidis(), dev);
        vector<string> instrs = al.getInstruments(midiNo);
        vector<string> instruments;
        const char *prefixes = "0123456789ABCDEFG";
        for(int i=0; i <= 16 && i < instrs.size(); ++i)
        {
            const char pp[3] = {prefixes[i], ' ', 0};
            string p = string(pp) + instrs[i];
            instruments.push_back(p);
        }
        instrList = guiI->addRadio("INSTRUMENTS", instruments);
        guiI->autoSizeToFitWidgets();
        guiI->setVisible(guiM->isVisible());
        ofAddListener(guiI->newGUIEvent,this,&Gui::guiEvent);
        cout <<"upd instr" << endl;
    }
}

//--------------------------------------------------------------
void Gui::activateRadioById(ofxUIRadio *r, int id)
{
    if (!r) return;
    vector<ofxUIToggle*> tt = r->getToggles();
    for (int i = 0; i < tt.size(); ++i)
    {
        string s = tt[i]->getName();
        int t;
        if (sscanf(s.c_str(), "%d", &t) && t == id)
        {
            r->activateToggle(s);
            return;
        }
    }
    // nothing found
    ofxUIToggle * a = r->getActive();
    if (a) a->setValue(false);
}

//--------------------------------------------------------------
void Gui::setMelodyPlaying(bool pl)
{
    play->setValue(pl);
    play->getLabelWidget()->setLabel(pl ? "PAUSE" : "PLAY");
}

//--------------------------------------------------------------
void Gui::guiEvent(ofxUIEventArgs &e)
{
    if (e.widget == play)
    {
        // ta->keyPressed('p');
        bool p = play->getValue();
        if (p) ta->play(); else ta->pause();
        play->getLabelWidget()->setLabel(p ? "PAUSE" : "PLAY");
    }
    else if (e.widget == pause)
    {
        ta->keyPressed(261); // F5
    }
    else if (e.widget == restoreBtn)
    {
        if(restoreBtn->getValue()) ta->keyPressed('Y');
    }
    else if (e.widget == reduceBtn)
    {
        if(reduceBtn->getValue()) ta->keyPressed('r');
    }
    else if (e.widget == splitBtn)
    {
        if(splitBtn->getValue()) ta->keyPressed('s');
    }
    else if (e.widget == mergeBtn)
    {
        if(mergeBtn->getValue()) ta->keyPressed('m');
    }
    else if (e.widget == pauseBtn)
    {
        if(pauseBtn->getValue()) ta->keyPressed('z');
    }
    else if (e.widget == rmPauseBtn)
    {
        if(rmPauseBtn->getValue()) ta->keyPressed('Z');
    }
    else if (e.widget == envelToggle)
    {
        if (envelToggle->getValue()) ta->keyPressed('L');
    }
    else if (e.widget == veloToggle)
    {
        if (veloToggle->getValue()) ta->keyPressed('V');
    }
    else if (e.widget == noteToggle)
    {
        if (noteToggle->getValue()) ta->keyPressed('N');
    }
    else if (e.widget == trackToggle)
    {
        if (trackToggle->getValue()) ta->keyPressed('K');
    }
    else if (e.widget == soloToggle)
    {
        if (soloToggle->getValue()) ta->keyPressed('o');
    }
    else if (e.widget == muteToggle)
    {
        if (muteToggle->getValue()) ta->keyPressed('u');
    }
    else if (e.widget == reverseToggle)
    {
        if (reverseToggle->getValue()) ta->keyPressed('b');
    }
    else if (e.widget == tickSlider)
    {
        float v = tickSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_TICK, v);
    }
    else if (e.widget == bpmSlider)
    {
        float v = bpmSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_BPM, v);
    }
    else if (e.widget == echoSlider)
    {
        float v = echoSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_ECHO, (int)v);
    }
    else if (e.widget == offsetSlider)
    {
        float v = offsetSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_TRANSPOSE, (int)v);
    }
    else if (e.widget == volumeSlider)
    {
        float v = volumeSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_VOLUME, (int)v);
    }
    else if (e.widget == dynamSlider)
    {
        float v = dynamSlider->getScaledValue();
        ta->setValue(VALUE_DYNAMIC, v);
    }
    else if (e.widget == qualitySlider)
    {
        float v = qualitySlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_QUALITY, v);
    }
    else if (e.widget == stacattoSlider)
    {
        float v = stacattoSlider->getScaledValue();
        ta->setValue(VALUE_DURATION, v);
    }
    else if (e.widget == drumSlider)
    {
        float v = drumSlider->getScaledValue() + 0.5;
        ta->setValue(VALUE_DRUM_COUNT, (int)v);
        updatePlaying();
    }
    else
    {
        string name = e.widget->getName();
        int kind = e.widget->getKind();
        ofxUIButton *button;
        ofxUIImageButton *ibutton;
        ofxUILabelButton *lbutton;
        ofxUIToggle *toggle;
        ofxUIImageToggle *itoggle;
        ofxUILabelToggle *ltoggle;
        ofxUISlider *slider;
        ofxOscMessage m;

        switch(kind)
        {
        case OFX_UI_WIDGET_SLIDER_H:
            slider = (ofxUISlider *) e.widget;
            cout << name << "\t value1: " << slider->getValue() << endl;
            break;
        case OFX_UI_WIDGET_BUTTON:
            button = (ofxUIButton *) e.widget;
            cout << name << "\t value2: " << button->getValue() << endl;
            break;
        case OFX_UI_WIDGET_TOGGLE:
            toggle = (ofxUIToggle *) e.widget;
            if (toggle->getValue())
            {
                if (e.widget->getParent() == destList)
                {
                    ta->setStrValue(VALUE_DEST, toggle->getName());
                }
                else if (e.widget->getParent() == chordList)
                {
                    string s = toggle->getName();
                    ta->setStrValue(VALUE_CHORD, s);
                }
                else if (e.widget->getParent() == midiList)
                {
                    ta->setStrValue(VALUE_MIDI, toggle->getName());
                    ta->midiNo = ofFind(al.getMidis(), toggle->getName());
                    updateInstrumentList(toggle->getName());
                    updateGui();
                }
                else if (e.widget->getParent() == instrList)
                {
                    ta->keyPressed(toggle->getName().at(0));
                }
                else if (e.widget->getParent() == drumList)
                {
                    int t;
                    if (sscanf(toggle->getName().c_str(), "%d", &t))
                    {
                        ta->setValue(VALUE_DRUM_TYPE, (int)(t - 1));
                        updatePlaying();
                    }
                }
                else if (e.widget->getParent() == playingList || e.widget->getParent() == drummingList)
                {
                    int id;
                    if (sscanf(toggle->getName().c_str(), "%d", &id))
                    {
                        ta->selectMelody(id);
                        updateGui(false);
                        if (e.widget->getParent() == drummingList) activateRadioById(playingList, id);
                        else activateRadioById(drummingList, id);
                    }
                }
                else if (e.widget->getParent() == recordsList)
                {
                    cout << toggle->getName() << endl;
                    ta->playRecord(toggle->getName());
                }
                else if (e.widget->getParent() == scalesList)
                {
                    cout << toggle->getName() << endl;
                    ta->scale(toggle->getName());
                }
                else
                    cout << name << "\t value3: " << toggle->getValue() << endl;
            }
            break;
        case OFX_UI_WIDGET_IMAGEBUTTON:
            ibutton = (ofxUIImageButton *) e.widget;
            cout << name << "\t value4: " << ibutton->getValue() << endl;
            break;
        case OFX_UI_WIDGET_IMAGETOGGLE:
            itoggle = (ofxUIImageToggle *) e.widget;
            cout << name << "\t value5: " << itoggle->getValue() << endl;
            break;
        case OFX_UI_WIDGET_LABELBUTTON:
            lbutton = (ofxUILabelButton *) e.widget;
            cout << name << "\t value6: " << lbutton->getValue() << endl;
            break;
        case OFX_UI_WIDGET_LABELTOGGLE:
            ltoggle = (ofxUILabelToggle *) e.widget;
            cout << name << "\t value7: " << ltoggle->getValue() << endl;
            break;
        case OFX_UI_WIDGET_DROPDOWNLIST:
            // cout << ((ofxUIDropDownList*) e.widget)->getName() << endl;
            break;
        case OFX_UI_WIDGET_SUPERCANVAS:
            // cout << e.widget->getName() << endl;
            accordion.event((ofxUISuperCanvas*) e.widget);
            // cout << ((ofxUIDropDownList*) e.widget)->getName() << endl;
            break;
        default:
            cout << kind << " " << name << endl;
        }
    }
}

void Accordion::addCanvas(ofxUISuperCanvas *sc)
{
    canvases.push_back(sc);
}

void Accordion::removeCanvas(ofxUISuperCanvas *sc)
{
    list<ofxUISuperCanvas *>::iterator it = canvases.begin();
    for( ; it != canvases.end(); it++)
    {
        if ((*it) == sc)
        {
            canvases.erase(it);
            break;
        }
    }
}

void Accordion::event(ofxUISuperCanvas *sc)
{
    if (incallback) return;
    cout << sc->getCanvasTitle()->getLabel() << endl;
    incallback = true;

    list<ofxUISuperCanvas *>::iterator it = canvases.begin();
    for(int i = 0; it != canvases.end(); it++, i++)
    {
        ofxUISuperCanvas *c= *it;
        if (c != sc || sc->isMinified())
        {
            c->setMinified(true);
            c->getRect()->x = 0;
            c->getRect()->y = i * CANVAS_HEIGHT;
        }
        else
        {
            c->getRect()->x = 200;
            c->getRect()->y = 0;
        }
    }
    incallback = false;
}

void Gui::setMelodyLabel(string lbl)
{
    guiM->getCanvasTitle()->setLabel(lbl);
}

void Gui::setValue(int key, float value, int mel)
{
    Melody *mp = ta->getMelody(mel);
    switch (key)
    {
    case VALUE_TICK:
        tickSlider->setValue(value);
        break;
    case VALUE_BPM:
        bpmSlider->setValue(value);
        break;
    case VALUE_ALPHA:
        // alpha = v; ignored for now
        break;
    case VALUE_DRUM_COUNT:
        if (mp) mp->m_DrumCount = value;
        if (mel == ta->melodyId)
        {
            drumSlider->setValue(value);
        }
        break;
    case VALUE_DRUM_TYPE:
        if (mp) mp->m_DrumType = value;
        if (mel == ta->melodyId)
        {
            drumList->activateToggle(al.getDrums()[(int)value]);
        }
        break;
    case VALUE_ECHO:
        if (mp) mp->setEcho(value);
        if (mel == ta->melodyId)
        {
            echoSlider->setValue(value);
        }
        break;
    case VALUE_DURATION:
        if (mp) mp->setLegato(value);
        if (mel == ta->melodyId)
        {
            stacattoSlider->setValue(value);
        }
        break;
    case VALUE_TRANSPOSE:
        if (mp) mp->setOffset(value);
        if (mel == ta->melodyId)
        {
            offsetSlider->setValue(value);
        }
        break;
    case VALUE_VOLUME:
        if (mp) mp->setVolume(value);
        if (mel == ta->melodyId)
        {
            volumeSlider->setValue(value);
        }
        break;
    case VALUE_DYNAMIC:
        if (mp) mp->setDynamic(value);
        if (mel == ta->melodyId)
        {
            dynamSlider->setValue(value);
        }
        break;
    case VALUE_QUALITY:
        if (mp) mp->quality = value;
        if (mel == ta->melodyId)
        {
            qualitySlider->setValue(value);
        }
        break;
    case VALUE_DISPLAY:
//        displayMode = v; ignored
        break;
    case VALUE_MIDI_CH:
        if (mp) mp->setCh(value);
        if (mel == ta->melodyId)
        {
            selectInstrument(value);
        }
        break;

    }

}

void Gui::setStrValue(int key, string str, int mel)
{
    Melody *mp = ta->getMelody(mel);
    switch(key)
    {
    case VALUE_DEST:
        if (mp) mp->setDest(str);
        if (mel == ta->melodyId)
        {
            destList->activateToggle(str);
        }
        break;
    case VALUE_CHORD:
        if (mp) mp->setChord(str);
        if (mel == ta->melodyId)
        {
            chordList->activateToggle(str);
        }
        break;
    }
}
