#ifndef GUI_H
#define GUI_H

#include "ofxIniSettings.h"
#include "ofxUI.h"
#include "AllLists.h"

class testApp;

class Accordion
{
    list<ofxUISuperCanvas*> canvases;
    bool incallback;
public:
    Accordion() { incallback = false; }
    void addCanvas(ofxUISuperCanvas *sc);
    void removeCanvas(ofxUISuperCanvas *sc);
    void event(ofxUISuperCanvas *sc);
};

class Gui
{
        Accordion accordion;
        AllLists al;
        bool show;
    public:
        Gui();
        virtual ~Gui();

        void init(testApp *a, ofxIniSettings &ini);
        void setMelodyLabel(string lbl);
        void setMelodyPlaying(bool pl);

        void setValue(int key, float value, int mel = - 1);
        void setStrValue(int key, string str, int mel = - 1);


        void guiEvent(ofxUIEventArgs &e);
        void updateGui(bool pl = true);
        void updatePlaying();
        void updateInstrumentList(string dev);
        void selectInstrument(int ch);

        bool isHit(float x, float y);
        void toggleVisible();
        void showMelodies(bool show);

    protected:
    private:
        ofxUISuperCanvas *guiM, *guiS, *guiI, *guiD, *guiG, *guiP, *guiR;
        ofxUILabel *melodyLabel;
        ofxUIToggle *play;
        ofxUIToggle *envelToggle;
        ofxUIToggle *veloToggle;
        ofxUIToggle *noteToggle;
        ofxUIToggle *trackToggle;
        ofxUIToggle *reverseToggle;
        ofxUIToggle *muteToggle;
        ofxUIToggle *soloToggle;
        ofxUIButton *reduceBtn;
        ofxUIButton *mergeBtn;
        ofxUIButton *splitBtn;
        ofxUIButton *pauseBtn;
        ofxUIButton *rmPauseBtn;
        ofxUIButton *restoreBtn;

        // Sliders
        ofxUISlider *drumSlider;
        ofxUISlider *echoSlider;
        ofxUISlider *offsetSlider;
        ofxUISlider *volumeSlider;
        ofxUISlider *dynamSlider;
        ofxUISlider *stacattoSlider;
        ofxUISlider *qualitySlider;

        ofxUIRadio *destList;
        ofxUIRadio *instrList;
        ofxUIRadio *chordList;
        ofxUIRadio  *drumList;

        // global
        ofxUIToggle *pause;
        ofxUISlider *tickSlider;
        ofxUISlider *bpmSlider;
        ofxUIRadio *midiList;
        ofxUIRadio *playingList;
        ofxUIRadio *drummingList;
        ofxUIRadio *recordsList;
        ofxUIRadio *scalesList;

        testApp *ta;
        string midi; // currently selected MIDI device. Its change triggers guiI reallocation
        int wd;

        void initCanvas(ofxUISuperCanvas *gg);
        void activateRadioById(ofxUIRadio *r, int id);
};

#endif // GUI_H
