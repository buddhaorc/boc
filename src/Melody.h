#ifndef MELODY_H
#define MELODY_H

#include "ofMain.h"
#include <list>
#include <string>
#include "ofxIniSettings.h"

// max number of MIDI channels
#define NCH 16
// Max number of rhythm patterns
#define MAXPTRN 50
// The shortest note 1/32
#define SHORTEST 32

// Melodies might be grouped in the controller.
// The expected size of a group is 3, for R,G, and B components
#define MAX_GROUP_SZ 10

// max number of MIDI sound modules
#define MAX_MIDI 8

class NoteDuration;

//class MidiInstruments
//{
//    static int m_Instrs[MAX_MIDI][NCH];
//    public:
//    static int m_InstrNum;
//    static int m_Effect;
//
//    static void assignChannels(int nm, MidiOutEx &mo, string &instr);
//    static void assignBank(int nm, MidiOutEx &mo, int ch, int b);
//    static const char *getName(int nm, int n, bool drum);
//    static int getProgram(int ch);
//};
//
struct CPoint
{
    CPoint(int xx, int yy) { x = xx; y = yy; }
    CPoint(const CPoint &p) { x = p.x; y = p.y; }

    void write(ostream &os);
    bool read(istream &os);

    int x;
    int y;
};

struct PtrnItem
{
    int vol;
    int dur;
    int cnt;
};

class Ptrn
{
        std::list<PtrnItem*> ptrn;
        std::list<PtrnItem*>::iterator now;
        int   rndVol;
        int   rndDur;
        char  title[100];
    public:
        Ptrn(string p);
        virtual ~Ptrn();
        PtrnItem *getNext();
        char *getTitle() { return title; };
};

class ofxIniSettings;

class PatternSet
{
        Ptrn *ptrns[MAXPTRN];
        int m_Cnt;
    public:
        PatternSet();
        void init(ofxIniSettings &ini);
        virtual ~PatternSet();

        int getNumber() { return m_Cnt; }
        Ptrn *getPtrn(int idx) { return ptrns[idx]; }
};

#define MPLAY 1
#define MDRUM 2
#define MINST 4

#define CC_VELO 1
#define CC_NOTE 2
#define CC_ENVE 4
#define CC_TRCK 8
#define CC_MUTE 16

class Melody
{
    public:
        Melody() : duration(0), paused(true),
            full(false), m_DrumCount(0), m_DrumType(0),
            effect(false), vibrato(false), portamento(false),
            inserted(false),
            quality(50), m_Ch0(-1), m_Bank(0), display(0), accord(0),
            ptrnIdx(0), m_Ptrn(0), reverse(false),
            volume(0), m_Offset(0), m_Dest(MIDI), m_legato(1.0f), ccFlags(0),
            m_Id(-1), m_ParentId(-1), m_HasChild(false), m_echo(0), m_dynamic(0.5) {}
        virtual ~Melody();

        void addPoint(CPoint *p) { points.push_back(p); }
        void setCenter(ofPoint &c);
        ofPoint getCenter() { return centroid; }

        void draw(float ox, float oy, float sx, float sy, bool line);
        int getDuration() { return duration; }

        // sets the current note to the position
        // closest to the given point
        void goTo(float x, float y);

        // drumming
        int m_DrumCount;
        int m_DrumType;
        bool isDrumming();
        int getPlayingState();
        bool getCCflag(int fl) { return (ccFlags & fl) != 0; }

        // volume adjustment
        int getVolume() { return volume; }
        void setVolume(int v) { volume = v; }

        void setDynamic(float d) { m_dynamic = d; }
        float getDynamic() { return m_dynamic; }

        int getImgWidth() { return m_ImgWidth; }
        int getImgHeight() { return m_ImgHeight; }

        Melody *copy(int id);

        bool control(int k);

        const char* info();
        const char* drumInfo();
        const char* instrInfo();
        int getCh() { return m_Ch0; }
        void setCh(int ch) { m_Ch0 = ch; }
        bool isDestOsc() { return m_Dest == OSC || m_Dest == BOTH; }
        static string getDestName(int i);
        string getDestName();
        void setDest(string nm);

        static string getChordName(int i);
        string getChordName();
        void setChord(string nm);

        int getEcho() { return m_echo; }
        void setEcho(int e) { m_echo = e; }

        int getOffset() { return m_Offset; }
        void setOffset(int o) { m_Offset = o; }

        float getLegato() { return m_legato; }
        void setLegato(float e) { m_legato = e; }

        bool getReverse() { return reverse; }
        void setReverse(bool r) { reverse = r; }

        void storeKey(int k);

        int m_Id;
        int m_ParentId;
        bool m_HasChild;

        // public because we want to show it on screen
        int quality; // probability of using "pure" notes of [0,4,7] set
        int display;

        enum GenType { ALL_POINTS, VERT_LINES, GRAVITY };
        static GenType gtype;

        static int echoDelay;
        static int echoVelocity;
        static int echoPitch;

        std::list<CPoint*> points;

        void stop() { paused = true; }
        void start() { paused = false; }

        void toggleCCflag(int fl);
        void ccFlagsInfo(char *buf);
    private:
        int m_Ch0; // the assigned midi channel
        int m_Ch; // last used midi channel
        int m_Bank;// selected MIDI bank
        bool paused;
        bool inserted; // in the short list
        bool full; // max volume
        bool drums; // plays in midi channel 10
        bool effect; // MidiInstruments::m_Effect
        bool vibrato;
        bool portamento;
        int accord; // 0 - off, 1 - up, 2 - down, 3 - random
        int duration; // total duration of all notes
        bool reverse;
        int volume; // adjustment -127..+127
        int m_Offset; // notes up-down
        float m_legato;
        string keystory;
        int ccFlags;
        static string storedKeys;

        ofRectangle     boundingRect;
        ofPoint         centroid;
        float           maxDist; // square
        int             m_ImgWidth;
        int             m_ImgHeight;

        enum { MIDI, OSC, BOTH, SERIAL } m_Dest;

        int m_echo; // number of echoes
        float m_dynamic;

        int  ptrnIdx;
        Ptrn *m_Ptrn;
};

struct Param
{
    std::string key;
    std::string val;
};

class Evolver
{
    public:
        Evolver(std::list<Param>);
        void evolve(Melody &src, Melody &dst);
};

#define MAXTOKEN 100

class MeloParser
{
    public:
        // possible parsing results:
        static const int OK;
        static const int EOM;
        static const int ERR;
        static const int CLOSE;
        static int nextKey(istream &is, char *buf);
        static int nextValue(istream &is, char *buf);
        static int nextInt(istream &is, const char *key, int &val);
        static int nextBool(istream &is, const char *key, bool &val);
};
#endif // MELODY_H
