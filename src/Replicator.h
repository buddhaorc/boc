#ifndef REPLICATOR_H
#define REPLICATOR_H

#include <map>
#include "ofxOsc.h"

class ofxIniSettings;
class testApp;

// don't send to the nodes which are older than this
#define MAX_BO_AGE 60000

class OscTarget
{
public:
    OscTarget() { sender = new ofxOscSender(); lastSeen = ofGetElapsedTimeMillis(); }
    ~OscTarget() { delete sender; }
    ofxOscSender *sender;
    long lastSeen;
    bool discovered;
    bool control; // true if boc; false if bo
};

class Replicator : public ofThread
{
    public:
        Replicator();
        virtual ~Replicator();

        void init(testApp *a, ofxIniSettings &ini);
        void invoke();
        void threadedFunction();

        // send messages to all remote hosts
        string key(int k);
        string key(int k, int mel);
        string selectMelody(int mel);
        string value(int k, float v, int mel);
        string svalue(int k, string s, int mel);
        string goTo(float x, float y);
        string midiControl(int control, int value);
        string midiNote(bool on, int ch, int pitch, int velo);
        string play(int id);
        string pause(int id);
        string loadImage(string s);
        string loadScale(string s);
        string loadStoredMelodies(string s);

        string myAddress() { return getThisIp() + ":" + ofToString(oscPort); }
        vector<string> getIfList();
        vector<string> getSenders();

        static string getThisIp();
        static string broadcast(string ip);
        static string oscToString(ofxOscMessage &m);

    protected:
    private:
        testApp *ta;
        int oscPort;
        int discoveryInterval;
        int maxAge;

        std::map<string, OscTarget*> senders;
        ofxOscSender discoverer;
        ofxOscReceiver receiver;

        string sendToAll(ofxOscMessage &m);
        ofxOscSender* updateSenders(string addr, bool discovered, bool control);
};

#endif // REPLICATOR_H
