#include "Replicator.h"
#include "testApp.h"
#include "Poco/Net/NetworkInterface.h"
#include "Poco/Net/DNS.h"
#include <limits.h>

using Poco::Net::IPAddress;
using Poco::Net::NetworkInterface;
using Poco::Net::HostEntry;
using Poco::Net::DNS;

Replicator::Replicator()
{
    //ctor
}

Replicator::~Replicator()
{
    //dtor
}

void Replicator::init(testApp *a, ofxIniSettings &ini)
{
    ta = a;

	oscPort = ini.get("oscPort", 0);
    if (oscPort) {
        cout << "Listening OSC at " << oscPort << endl;
        receiver.setup(oscPort);
    }

    vector<string> ips;
    NetworkInterface::NetworkInterfaceList ifcs = NetworkInterface::list();
    for (int i=0; i<ifcs.size(); ++i)
    {
        NetworkInterface ni = ifcs[i];
        IPAddress ip = ni.address();
        cout << ip.toString() << " " << ni.displayName() << endl;
        ips.push_back(ip.toString());
    }

    string repstr = ini.get("remote", string(""));
    vector<string> reps = ofSplitString(repstr, ";", true, true);
    for (int i=0; i<reps.size(); ++i) {
        string dest = reps[i];
        OscTarget *target = new OscTarget();
        int sep = dest.find(':');
        if (sep != string::npos)
        {
            string hostname = dest.substr(0, sep);
            string p = dest.substr(sep + 1);
            int port;
            if (sscanf(p.c_str(), "%d", &port) == 1)
            {
                if (ofContains(ips, hostname) && port == oscPort)
                {
                    cout << "excluded " << hostname << ":" << port << endl;
                    continue;
                }
                target->sender->setup(hostname, port);
                senders[hostname + ":" + ofToString(port)] = target;
            }
            else
            {
                cout << "failed sscanf " << p << endl;
            }
        }
        else
        {
            if (ofContains(ips, dest))
            {
                cout << "excluded " << dest << endl;
                continue;
            }
            target->sender->setup(dest, oscPort);
            senders[dest + ":" + ofToString(oscPort)] = target;
        }
    }

    if (ini.get("discovery", false))
    {
        discoveryInterval = ini.get("discoveryInterval", 10000);
        discoverer.setup(broadcast(getThisIp()), oscPort);
        startThread(true, false);
    }
    else
    {
        discoveryInterval = INT_MAX;
    }

    maxAge = ini.get("maxAge", MAX_BO_AGE);
    // we cannot be too strict
    if (maxAge < discoveryInterval * 2) maxAge = discoveryInterval * 2;
}

string Replicator::key(int k)
{
    ofxOscMessage m;
    m.setAddress( "/bo/key" );
    m.addIntArg( k );
    return sendToAll(m);
}

string Replicator::key(int k, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/key" );
    m.addIntArg( k );
    m.addIntArg( mel );
    return sendToAll(m);
}

string Replicator::selectMelody(int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/select" );
    m.addIntArg( mel );
    return sendToAll(m);
}

string Replicator::value(int k, float v, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/value" );
    m.addIntArg( k );
    m.addIntArg( mel );
    m.addFloatArg( v );
    return sendToAll(m);
}

string Replicator::svalue(int k, string s, int mel)
{
    ofxOscMessage m;
    m.setAddress( "/bo/svalue" );
    m.addIntArg( k );
    m.addIntArg( mel );
    m.addStringArg( s );
    return sendToAll(m);
}

string Replicator::goTo(float x, float y)
{
    ofxOscMessage m;
    m.setAddress( "/bo/goto" );
    m.addFloatArg( x );
    m.addFloatArg( y );
    return sendToAll(m);
}

string Replicator::midiControl(int control, int value)
{
    ofxOscMessage m;
    m.setAddress( "/bo/midi/control" );
    m.addIntArg( control );
    m.addIntArg( value );
    return sendToAll(m);
}

string Replicator::midiNote(bool on, int ch, int pitch, int velo)
{
    ofxOscMessage m;
    m.setAddress( "/bo/midi/note" );
    m.addIntArg( on ? 1 : 0 );
    m.addIntArg( ch );
    m.addIntArg( pitch );
    m.addIntArg( velo );
    return sendToAll(m);
}

string Replicator::loadImage(string s)
{
    ofxOscMessage m;
    m.setAddress( "/bo/image" );
    m.addStringArg( s );
    return sendToAll(m);
}

string Replicator::loadScale(string s)
{
    ofxOscMessage m;
    m.setAddress( "/bo/scale" );
    m.addStringArg( s );
    return sendToAll(m);
}

string Replicator::loadStoredMelodies(string s)
{
    ofxOscMessage m;
    m.setAddress( "/bo/melodies" );
    m.addStringArg( s );
    return sendToAll(m);
}

string Replicator::sendToAll(ofxOscMessage &m)
{
    long oldest = ofGetElapsedTimeMillis() - maxAge;
    lock();
    for (map<string, OscTarget*>::iterator sit = senders.begin(); sit != senders.end(); sit++)
    {
        if (sit->second->lastSeen > oldest)
        {
            ofxOscSender *s = sit->second->sender;
            s->sendMessage( m );
        }
    }
    unlock();
    return oscToString(m);
}

string Replicator::play(int id)
{
    ofxOscMessage m;
    m.setAddress( "/bo/melody/play" );
    m.addIntArg( id );
    return sendToAll( m );
}

string Replicator::pause(int id)
{
    ofxOscMessage m;
    m.setAddress( "/bo/melody/pause" );
    m.addIntArg( id );
    return sendToAll( m );
}

string Replicator::oscToString(ofxOscMessage &m)
{
    string a = m.getAddress();
    for (int i = 0; i < m.getNumArgs(); i++)
    {
        a += " ";
        switch(m.getArgType(i))
        {
        case OFXOSC_TYPE_INT32:
            a += ofToString(m.getArgAsInt32(i));
            break;
        case OFXOSC_TYPE_FLOAT:
            a += ofToString(m.getArgAsFloat(i), 2);
            break;
        case OFXOSC_TYPE_STRING:
        default:
            a += m.getArgAsString(i);
            break;
        }
    }
    return a;
}

// handles all incoming messages
void Replicator::invoke()
{
    string myAddr = myAddress();
    // check for waiting messages
    while(receiver.hasWaitingMessages()){
        ofxOscMessage m;
        receiver.getNextMessage(&m);

        if (m.getAddress() == "/bo/discovery")
        {
            // this is a discovery requst
            string from = m.getArgAsString(0);
            if (from != myAddr)
            {
                ta->info("discovery from " + from);
                ofxOscMessage resp;
                resp.setAddress("/bo/boc");
                resp.addStringArg(myAddr);
                // send response
                ofxOscSender *sender = updateSenders(from, false, false);
                sender->sendMessage(resp);
            }
        }
        else if (m.getAddress() == "/bo/bo")
        {
            // this is a discovery response
            ta->info("bo " + m.getArgAsString(0));
            updateSenders(m.getArgAsString(0), true, false);
        }
        else if (m.getAddress() == "/bo/boc")
        {
            // this is a discovery response
            ta->info("control " + m.getArgAsString(0));
            updateSenders(m.getArgAsString(0), true, true);
        }
        else if (!ta->newOscMessage(m))
        {
            ta->error("not handled " + oscToString(m));
            cout << "not handled " << oscToString(m) << endl;
        }
    }
}

ofxOscSender* Replicator::updateSenders(string addr, bool discovered, bool control)
{
    ofxOscSender* snd = 0;
    lock();
    map<string, OscTarget*>::iterator it = senders.find(addr);
    OscTarget *ot = 0;
    if (it == senders.end())
    {
        // not found
        ot = new OscTarget();
        ot->lastSeen = ofGetElapsedTimeMillis();
        vector<string> aa = ofSplitString(addr, ":", true, true);
        if (aa.size() == 2)
        {
            ot->sender->setup(aa[0], ofToInt(aa[1]));
        }
        else
        {
            ot->sender->setup(addr, oscPort);
        }
        senders[addr] = ot;
        if (discovered) ot->discovered = true;
        snd = ot->sender;
    }
    else
    {
        ot = it->second;
        ot->lastSeen = ofGetElapsedTimeMillis();
        snd = ot->sender;
    }

    if (discovered)
    {
        ot->discovered = true;
        ot->control = control;
    }
    unlock();
    return snd;
}

string Replicator::getThisIp()
{
    HostEntry he = DNS::thisHost();
    IPAddress ad = DNS::resolveOne(he.name());
    return ad.toString();
}

string Replicator::broadcast(string ip)
{
    size_t dot = ip.rfind('.');
    if (dot == string::npos) {
        cout << "possible broadcast problem: " << ip << endl;
        return ip;
    }
    string s = ip.substr(0, dot + 1) + "255";
//    cout << "broadcast " << s << endl;
    return s;
}

void Replicator::threadedFunction() {
    while (isThreadRunning())
    {
        ofxOscMessage m;
        m.setAddress("/bo/discovery");
        m.addStringArg(myAddress());
        discoverer.sendMessage(m);
        for (int w = discoveryInterval; w > 0 && isThreadRunning(); )
        {
            int wt = w > 500 ? 500 : w;
            w -= wt;
            ofSleepMillis(wt);
        }
    }
}

vector<string> Replicator::getIfList()
{
    vector<string> ips;
    NetworkInterface::NetworkInterfaceList ifcs = NetworkInterface::list();
    for (int i=0; i<ifcs.size(); ++i)
    {
        NetworkInterface ni = ifcs[i];
        IPAddress ip = ni.address();
        ips.push_back(ip.toString() + " " + ni.displayName());
    }
    return ips;
}

vector<string> Replicator::getSenders()
{
    long now = ofGetElapsedTimeMillis();
    long oldest = now - maxAge;

    vector<string> ss;
    vector<string> olds;
    lock();
    for (map<string, OscTarget*>::iterator sit = senders.begin(); sit != senders.end(); sit++)
    {
        OscTarget * t = sit->second;
        long age = (now - t->lastSeen) / 1000;
        string state = t->discovered ? (t->control ? "BO Control" : "BO") : "?";

        ostringstream out;
        out << sit->first << " " << state << " " << age << " sec";
        if (t->lastSeen > oldest)
        {
            ss.push_back( out.str() );
        }
        else
        {
            olds.push_back( out.str() );
        }
        cout << out.str() << endl;
    }
    unlock();
    if (olds.size())
    {
        ss.push_back("--- INACTIVE ---");
        ss.insert(ss.end(), olds.begin(), olds.end());
    }
    return ss;
}
