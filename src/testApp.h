#pragma once

#include "ofMain.h"
#include "ofxUI.h"
#include "ofxOsc.h"
#include "Gui.h"
#include "Replicator.h"
#include "Route.h"
#include "Melody.h"

#define VALUE_TICK  1
#define VALUE_BPM   2
#define VALUE_DRUM_COUNT    3
#define VALUE_DRUM_TYPE     4
#define VALUE_ECHO  5
#define VALUE_DURATION  6
#define VALUE_TRANSPOSE 7
#define VALUE_VOLUME    8
#define VALUE_QUALITY   9
#define VALUE_DEST  10
#define VALUE_CHORD 11
#define VALUE_MIDI  12
#define VALUE_ALPHA 13
#define VALUE_DISPLAY 14
#define VALUE_DYNAMIC 15
#define VALUE_MIDI_CH 16

#define MAX_MELODIES 100

class testApp;
class RecordPlayer : public ofThread
{
        list<string> records;
        testApp *ta;
        long t0, totalTime;
        int currentLn, totalLn;
        char nextAct;
        int nextActTime;
        string cmd;

        bool readNext(ifstream &is);
        void scanfile(ifstream &is);
    public:
        RecordPlayer() {}
        virtual ~RecordPlayer() {}
        void init(testApp* t) { ta = t; }
        void addRecord(string rec) { lock(); records.push_back(rec); unlock(); }
        void threadedFunction();
        void playRecord(string r);
};

class testApp : public ofBaseApp{

	Gui gui;
	Replicator replicator;
	ofImage img;
	Routes routes;
	RecordPlayer player;
	string lastCmd;
	Melody melodies[MAX_MELODIES];

	public:
		void setup();
		void update();
		void draw();
        void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    	void guiEvent(ofxUIEventArgs &e);

        void setValue(int key, float v, int me = -1);
        void setStrValue(int key, string s, int me = -1);
        void selectMelody(int id);
        void play();
        void pause();
        void midiControl(int control, int value);
        void midiNote(bool on, int ch, int pitch, int velo);
        void loadImage(string s);
        void loadStoredMelodies(string s);
        void goTo(float x, float y);

        bool newOscMessage(ofxOscMessage& eventArgs);

        void error(string e);
        void info(string e);

        void playRecord(string recname);
        void scale(string scname);
        void updateGui(bool pl) { gui.updateGui(pl); }

        Melody *getMelody(int id) { return id < MAX_MELODIES && id >= 0 ? &melodies[id] : 0; }
        // currently selected, -1 if none
        int melodyId;
        int midiNo; // selected
        bool solo; // one melody is playing solo
        bool paused; // all stopped with F5

        string playingInfo;
};
