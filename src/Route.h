#ifndef ROUTE_H
#define ROUTE_H

#include "AllLists.h"

#define MAX_ROUTES 100

#define BOTTOM 20

class Route
{
    public:
        Route();
        Route(int id);
        virtual ~Route();
        bool load(istream &is);
        void draw(float sx, float sy, bool selected, bool mouse, bool fill);

        bool reset(int id, int col, ofPoint center);
        void addPoint(float x, float y);

        int m_Id;
        int m_col;
        ofPoint m_center;
    protected:
    private:
        ofPolyline poly;
};

class Routes
{
    public:
        Routes();
        virtual ~Routes();
        bool load(const char* fn);
        void draw(float w, float h);
        string validateHeader(int wd, int ht);
        string setRouteHeader(int id, int col, ofPoint center);
        string addRoutePoint(int id, float x, float y);

        int m_selected, m_mouse;
        const char *getImageName() { return imageFileName.c_str(); }
        void mouseOver(int x, int y);
        int mousePressed(int x, int y);
        Route *getRoute(int id = -1) { if (id <0) id = m_selected; return id < routes.size() ? routes[id] : 0; }
    protected:
    private:
        float imgW, imgH;
        string imageFileName;
        vector<Route*> routes;
        int selected;
        int closest(int x, int y);
};

#endif // ROUTE_H
