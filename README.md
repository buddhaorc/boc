Buddha Orchestra Control
========================

There will be two versions:

 *  OSC-based can only send commands to BO but cannot get the current status - for example if the selected melody is playing, etc
 *  HTTP-based can send commands and show the status of BO

To compile the HTTP version, add the define in Build options: HTTP_TRANSPORT

OSC Commands
============

    /bo/key  (int key)
    /bo/select  (int melodyId)
    /bo/value  (int type, float value, int melodyId)
    /bo/svalue  (int type, string str, int melodyId)
    /bo/goto  (float x, float y)
    /bo/midi/control (int control, int value)
    /bo/midi/note (int on, int channel, int pitch, int velocity) ; on = 1 for Note On, = 0 for Note Off
    /bo/image (string filename)
    /bo/melodies (string filename)
